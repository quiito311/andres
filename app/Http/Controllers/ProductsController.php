<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Products;

class ProductsController extends Controller
{
    //
    public function registerProducts(Request $request){
        $data = $request->all();
        $products = new Products($data); //Nombre del modelo es el que se coloca aqui
        $products->save();
        return Redirect::to('/products');
    }
}
