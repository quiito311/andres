<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table ="products";
    protected $fillable = [
        'id',
        'name',
        'description',
        'price',
        'user_id',
        'image_id',
        'location_id',
        'barcode_id',
        'signature_state'
        //       
    ];
}
